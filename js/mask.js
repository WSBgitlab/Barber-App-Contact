const mask = {
    cpf(value){
        return value
        .replace(/\D/g,'')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace((/(\d{3})(\d{1,2})$/), '$1-$2')
    },
    email(value){
        return value
        .replace(/([a-zA-Z]+@)/, '$1')
    },
    telefone(value){
        return value
        .replace(/\D/g, '')
        .replace(/(\d{2})(\d)/, '($1) $2')
        .replace(/(\d{4})(\d)/, '$1-$2')
        .replace(/(\d{4})-(\d)(\d{4})/, '$1$2-$3')
    }
}

document.querySelectorAll('input').forEach(($input) =>{
    const field = $input.dataset.js

    $input.addEventListener('input', e =>{
        e.target.value = mask[field](e.target.value)
    }, false)
});


