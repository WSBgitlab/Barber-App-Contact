const buttonForm = document.getElementById('buttonForm');


buttonForm.addEventListener('click', (e) => {
    const name = document.getElementById('form-name').value;
    const tel = document.getElementById('form-tel').value;
    const email = document.getElementById('form-email').value;
    const social = document.getElementById('form-social').value;
    
    e.preventDefault();

    if(name === ""){
        document.getElementById('warning-name').style.display = "block";
        document.getElementById('warning-name').innerHTML = "* O campo Nome dever ser preenchido!";
    }

    if(tel === ""){
        document.getElementById('warning-phone').style.display = "block";
        document.getElementById('warning-phone').innerHTML = "* O campo Telefone dever ser preenchido!";
    }

    if(email === ""){
        document.getElementById('warning-email').style.display = "block";
        document.getElementById('warning-email').innerHTML = "* O campo E-mail dever ser preenchido!";
    }

});

